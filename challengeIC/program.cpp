#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;
// Functions

vector<int> readNum(string &s) {
	vector<int> vec;
	s.push_back(' ');
	if (s.size() == 1)
	{
		return vec;
	}
	unsigned int aux = s.find(' ');
	vec.push_back(stoi(s.substr(0, aux).c_str()));
	int pos = s.find(' ', aux + 1);
	while (pos != -1)
	{
		vec.push_back(stoi(s.substr(aux + 1, pos - (aux + 1))));
		aux = s.find(' ', aux + 1);
		pos = s.find(' ', aux + 1);
	}

	return vec;
}

vector<int> findBiggest(vector<int> vec)
{
	int max = -1;
	int pos;
	vector<int> positions;
	vector<int> aux = vec;
	for (int j = 0; j < vec.size(); j++)
	{
		max = -1;
		pos = 0;
		for (int i = 0; i < aux.size(); i++)
		{
			if (aux[i] > max)
			{
				max = aux[i];
				aux[i] = -1;
				pos = i;
			}
		}
		positions.push_back(pos);
	}
	return positions;
}

bool compare(vector<int> a, vector<int> b)
{
	for (int i = 0; i < a.size() && i < b.size(); i++)
	{
		if (a[i] != b[i])
			return false;
	}
	return true;
}

// Main execution

int main(int argc, char *argv[]) {
	string line;
	
	while (getline(cin, line))
	{
		vector<int> num = readNum(line);
		getline(cin, line);
		vector<int> permutation = readNum(line);
		vector<int> aux = permutation;
		vector<int> big;
		int n = num[1];
		for (int i = 0; i < n && i < num[0]; i++)
		{
			big = findBiggest(permutation);
			swap(permutation[i], permutation[big[i]]);
			if (compare(aux, permutation))
			{
				n++;
			}	
			aux = permutation;
		}
		for (int i = 0; i < permutation.size() - 1; i++)
		{
			cout << permutation[i] << " ";
		}
		cout << permutation[permutation.size() - 1] << endl;
	}
		
	return 0;

}